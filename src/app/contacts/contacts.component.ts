import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  message!: string;
  emailUser!: string;

  modalOpen = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  openModal() {
    this.modalOpen = true;
  }

  closeModal() {
    this.modalOpen = false;
  }

  sendMessage() {
    void this.router.navigate(['/send/message']);
  }

}
