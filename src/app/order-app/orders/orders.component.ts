import {Component, Input} from '@angular/core';
import { Product } from '../../shared/product.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent  {
  @Input()  products: Product[] = [];

  deleteItem(index:number){
   this.products[index].quantity = 0;
 }

  getTotalPrice() {
   return this.products.reduce( (sum, product) => {
    return  sum + product.price;
   }, 0);
 }
}
