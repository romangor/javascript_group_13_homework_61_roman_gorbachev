import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MyProjectsComponent } from './my-projects/my-projects.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { ContactsComponent } from './contacts/contacts.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { OrdersComponent } from './order-app/orders/orders.component';
import { ProductsComponent } from './order-app/products/products.component';
import { OrderAppComponent } from './order-app/order-app.component';
import { ProjectsService } from './shared/projects.service';
import { UserAppComponent } from './users-app/user-app.component';
import { NewUserComponent } from './users-app/new-user/new-user.component';
import { UsersComponent } from './users-app/users/users.component';
import { UserComponent } from './users-app/users/user/user.component';
import { UsersService } from './shared/users.service';
import { NewGroupComponent } from './users-app/new-group/new-group.component';
import { GroupsComponent } from './users-app/groups/groups.component';
import { GroupService } from './shared/group.service';
import { GroupComponent } from './users-app/groups/group/group.component';
import { ModalComponent } from './ui/modal/modal/modal.component';
import { FormsModule } from '@angular/forms';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'my-projects', component: MyProjectsComponent},
  {path: 'about-me', component: AboutMeComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: 'app/orders', component: OrderAppComponent},
  {path: 'app/user', component: UserAppComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    MyProjectsComponent,
    AboutMeComponent,
    ContactsComponent,
    HomeComponent,
    ProductsComponent,
    OrdersComponent,
    OrderAppComponent,
    UserAppComponent,
    NewUserComponent,
    UsersComponent,
    UserComponent,
    NewGroupComponent,
    GroupsComponent,
    GroupComponent,
    ModalComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ProjectsService, GroupService, UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
