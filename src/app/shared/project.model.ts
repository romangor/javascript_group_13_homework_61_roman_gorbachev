export class ProjectModel {
  constructor(public title: string,
              public description: string,
              public urlImg: string,
              public path: string,) {
  }
}
