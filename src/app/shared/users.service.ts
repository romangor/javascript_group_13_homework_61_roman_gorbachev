import { User } from './user.model';
import { EventEmitter } from '@angular/core';

export class UsersService {
  userChange = new EventEmitter<User[]>();

 private users: User[] = [
   new User ('Jhon', 'test@mail.com', true, 'Admin'),
   new User ('Jhon', 'test1@mail.com', false, 'Editor'),
 ];

 getUsers() {
   return this.users.slice()
 }

 addUser(user: User) {
   this.users.push(user);
   this.userChange.emit(this.users);
 }


}
