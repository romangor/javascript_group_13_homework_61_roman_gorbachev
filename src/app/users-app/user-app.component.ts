import { Component } from '@angular/core';

@Component({
  selector: 'app-user-app',
  templateUrl: './user-app.component.html',
  styleUrls: ['./user-app.component.css']
})
export class UserAppComponent  {

  show = true;

  showFormsManage(){
    this.show = false;
  }

  showFormGroups() {
    this.show = true;
  }
}
