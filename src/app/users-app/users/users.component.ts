import { Component } from '@angular/core';
import { User } from '../../shared/user.model';
import { UsersService } from '../../shared/users.service';


interface onInit {
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements onInit {
  users!: User[];

  constructor(private usersService: UsersService) {}

  ngOnInit() {
    this.users = this.usersService.getUsers();
    this.usersService.userChange.subscribe((users: User[]) => {
      this.users = users;
    });
  }

}
