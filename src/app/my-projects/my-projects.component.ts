import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectModel } from '../shared/project.model';
import { ProjectsService } from '../shared/projects.service';


@Component({
  selector: 'app-my-projects',
  templateUrl: './my-projects.component.html',
  styleUrls: ['./my-projects.component.css']
})
export class MyProjectsComponent implements OnInit {
  projects!: ProjectModel[];

  constructor(private router: Router, private projectsService: ProjectsService) { }

  ngOnInit(): void {
    this.projects = this.projectsService.getProjects();
  }

  showProject(path: string): void {
   void this.router.navigate([path])
  }

}
